﻿using Command;
using Input_Reader;
using Observer;
using UnityEngine;

[RequireComponent(typeof (MouseInput_Reader))]
[RequireComponent(typeof (KeyboardInput_Reader))]
[RequireComponent(typeof (CommandProcessor))]
public class Entity : Player, IEntity
{
    MouseInput_Reader mouseInput;
    KeyboardInput_Reader keyboardInput;

    CommandProcessor commandProcessor;

    Camera mainCamera;
    float cameraSmoothSpeed = 1f;
    Vector3 camdistance = new Vector3(0f, 18f, -5f);


    public Transform aim;

    public Transform shotStartPoint;
    Transform IEntity.aim => aim;

    protected Rigidbody rb;
    float cameraSpeed, cameraX, cameraY;
    bool moovingCamera;
    protected new virtual void Awake()
    {
        cameraSpeed = 5f;
        mouseInput = GetComponent<MouseInput_Reader>();
        keyboardInput = GetComponent<KeyboardInput_Reader>();
        commandProcessor = GetComponent<CommandProcessor>();
        mainCamera = Camera.main;
        rb = GetComponent<Rigidbody>();
        base.Awake();
        mainCamera.transform.LookAt(transform.position);
    }
    
    protected virtual void Start() {

    }
    protected virtual void Update()
    {
        //keyboard
        Vector3 moveDirection = keyboardInput.ReadMoveInput();
        if (moveDirection != Vector3.zero) {
            OnPlayerMove();
            var moveComand = new MoveCommand(this, moveDirection);
            commandProcessor.ExecuteCommand(moveComand);
        }
        /*
        if (keyboardInput.ReadThrowInput())
        {
        }*/
        if (keyboardInput.ReadChangeWeaponInput())
        {
            var changeWeapponCommand = new ChangeWeaponCommand(this, staff, bow, GetComponent<PhotonPlayer>());
            OnChangeWeapon();
            commandProcessor.ExecuteCommand(changeWeapponCommand);
        }
        //mouse
        ClickType click = mouseInput.GetClickStatus();

        if (click == ClickType.holding) {
            aim.transform.GetChild(0).gameObject.SetActive(true);
        }
        else if (click == ClickType.pressed) {
            var shotCommand = new ShotCommand(this, GetComponent<PhotonPlayer>());
            commandProcessor.ExecuteCommand(shotCommand);
        }
        else {
            aim.transform.GetChild(0).gameObject.SetActive(false);
        }

        if (mouseInput.GetRightClick())
        {
            moovingCamera = true;
            cameraX = Input.GetAxis("Mouse X") * cameraSpeed;
            mainCamera.transform.Rotate(Vector3.forward, cameraX);
        }
        else {
            moovingCamera = false;
        }

        Vector3 Shotdirection = mouseInput.GetClickPosition();
        if (Shotdirection != Vector3.zero)
        {
            var rotateAimCommand = new RotateAimCommand(this, Shotdirection);
            commandProcessor.ExecuteCommand(rotateAimCommand);
        }
        //
    }
    protected virtual void LateUpdate() {
            MoveCamera();
    }

    void MoveCamera() {
        Vector3 distance = transform.position + camdistance;
        mainCamera.transform.position = Vector3.Lerp(mainCamera.transform.position, distance, cameraSmoothSpeed * Time.deltaTime);
        if (!moovingCamera) { 
            mainCamera.transform.LookAt(transform.position);
        }
    }
}
