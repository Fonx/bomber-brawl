﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class Penguin : MonoBehaviour
{
    PhotonView PV;
    GameObject player;
    enum State { Idle, Roaming, Attack };
    State currentState;
    protected Vector3 targetPosition;
    Animator anim;
    Rigidbody rb;
    protected Vector3 direction;
    public float speed;
    bool firstFrame;

    public float distanceToLure;
    public Collider col;
    protected void Start()
    {
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody>();
        PV = GetComponent<PhotonView>();
        col.enabled = false;
        Invoke("EnableCollider", 1f);
        InvokeRepeating("ChangecurrentState", 2.5f, 2.5f);
        player = GameObject.FindWithTag("Player");//TRASH
    }
    public void EnableCollider()
    {
        col.enabled = true;
    }
    protected void Update()
    {
        CheckVelocity();
        if (currentState == State.Idle)
        {
            rb.velocity = Vector3.zero;
        }
        else if (currentState == State.Roaming)
        {
            if (firstFrame)
            {
                targetPosition = (new Vector3(transform.position.x + Random.Range(-3, 3), 0,
                    transform.position.z + Random.Range(-3, 3))).normalized;
                rb.velocity = Vector3.zero;
                Vector3 lookPos = targetPosition - transform.position;
                lookPos.y = 0;
                Quaternion rotation = Quaternion.LookRotation(lookPos);
                transform.rotation = Quaternion.Slerp(transform.rotation, rotation, 0.015f);
            }

            transform.Translate(targetPosition * 0.015f, Space.World);
        }
        else if (currentState == State.Attack)
        {
            targetPosition = player.transform.position;
            direction = targetPosition - transform.position;
            transform.LookAt(player.transform.position);
            rb.AddForce(new Vector3(direction.x, 0, direction.z) * speed);
        }
        firstFrame = false;
    }
    protected void CheckVelocity()
    {
        if (rb.velocity.magnitude > 26)
        {
            Die();
        }
    }
    public void ChangecurrentState()
    {
        int chance = Random.Range(1, 4);
        if (chance == 1)
        {
            if (CheckIfIsNearby())
                currentState = State.Attack;
            else
                currentState = State.Idle;
        }
        else if (chance == 2)
        {
            currentState = State.Idle;
        }
        else if (chance == 3)
        {
            currentState = State.Roaming;
        }
        firstFrame = true;
    }
    public bool CheckIfIsNearby()
    {
        if ((transform.position - player.transform.position).magnitude < distanceToLure)
            return true;
        else
            return false;
    }
    protected void LateUpdate()
    {
        //LEGACY É MUITO FEIO MELHOR NEM USAR ANIMAÇÃO
        /*
        if (currentState == State.Idle)
        {
            anim.clip = anim.GetClip("idle");
            anim.Play();
        }
        else if (currentState == State.Roaming)
        {
            anim.clip = anim.GetClip("walk");
            anim.Play();
        }
        else if (currentState == State.Attack)
        {
            anim.clip = anim.GetClip("run");
            anim.Play();
        }
        */
        if (currentState == State.Idle)
        {
            anim.SetBool("Idle", true);
            anim.SetBool("Walk", false);
            anim.SetBool("Run", false);
        }
        else if (currentState == State.Roaming)
        {
            anim.SetBool("Idle", false);
            anim.SetBool("Walk", true);
            anim.SetBool("Run", false);
        }
        else if (currentState == State.Attack)
        {
            anim.SetBool("Idle", false);
            anim.SetBool("Walk", false);
            anim.SetBool("Run", true);
        }
    }

    public void ChangeStateTOATk()
    {
        currentState = State.Attack;
    }

    public void Die()
    {
        PhotonNetwork.Destroy(gameObject);
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Damageable")) {
            Die();
        }
    }
}
