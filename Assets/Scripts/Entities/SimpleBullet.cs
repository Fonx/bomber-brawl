﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleBullet : MonoBehaviour
{
    public float speed = 0.05f;
    private void Start()
    {
        Destroy(gameObject, 3f);
    }
    void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, transform.position + transform.forward, speed);
    }
}
