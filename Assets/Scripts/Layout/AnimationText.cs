﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class AnimationText : MonoBehaviour
{
    int time = 3;
    public TMP_Text text;
    public GameObject gameName;
    public void TrigTime() {
        time--;
        text.text = time.ToString();
        if (time == 0) {
            text.text = "Brawl";
        }
        else if (time == -1) {
            gameName.SetActive(false);
            Destroy(gameObject);
        }
    }
}
