﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class PlayerStats : MonoBehaviour
{
    int playerID;
    int score = 0;

    [SerializeField] TMP_Text playerName;
    [SerializeField] TMP_Text playerScore;

    public int PlayerID => playerID;

    public void Initialize(int playerID) {
        this.playerID = playerID;
        string playerName = "Player " + playerID;
        this.playerName.text = playerName;
        this.playerScore.text = "0";
    }

    public void UpdateScore(int pts) {
        score += pts;
        playerScore.text = score.ToString();
    }
}
