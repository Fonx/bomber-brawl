﻿using UnityEngine;

namespace Input_Reader
{
    public class KeyboardInput_Reader : MonoBehaviour
    {
        public Vector3 ReadMoveInput()
        {
            float x = 0, y = 0;
            if (Input.GetKey(KeyCode.A))
                x = -1;
            else if (Input.GetKey(KeyCode.D))
                x = 1;

            if (Input.GetKey(KeyCode.W))
                y = 1;
            else if (Input.GetKey(KeyCode.S))
                y = -1;
            if (x != 0 || y != 0)
            {
                Vector3 dir = new Vector3(x, 0, y);
                return dir;
            }
            return Vector3.zero;
        }
        public bool ReadThrowInput()
        {
            if (Input.GetKey(KeyCode.G))
            {
                return true;
            }
            return false;
        }
        public bool ReadChangeWeaponInput()
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                return true;
            }
            return false;
        }
    }
}