﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Input_Reader
{
    public class MouseInput_Reader : MonoBehaviour
    {
        Camera mainCamera;
        private ClickType click;
        private void Awake()
        {
            mainCamera = Camera.main;
        }
        public Vector3 GetClickPosition()
        {
            RaycastHit hitInfo;
            Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hitInfo))
            {
                return hitInfo.point;
            }
            return Vector3.zero;
        }

        public ClickType GetClickStatus()
        {
            if (Input.GetKey(KeyCode.Mouse0))
            {
                if (click == ClickType.idle)
                {
                    click = ClickType.holding;
                }
                return click;
            }
            if (click == ClickType.holding)
            {
                click = ClickType.pressed;
            }
            else if (click == ClickType.pressed)
            {
                click = ClickType.idle;
            }
            return click;
        }
        public bool GetRightClick() {
            if (Input.GetKey(KeyCode.Mouse1)) {
                return true;
            }
            return false;
        }

    }
}
public enum ClickType {idle,holding,pressed }
