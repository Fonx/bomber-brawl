﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Photon.Pun;
using Photon.Realtime;
using System.IO;

public class PhotonRoom : MonoBehaviourPunCallbacks, IInRoomCallbacks
{
    private PhotonView PV;
    public int currentScene;
    //public ApiWebController api;
    public List<Session> sessionsOnline;
    public List<PhotonPlayer> playersOnline;
    public PhotonPlayer myAvatar;
    public string newestPlayerName;
    public string newestPlayerId;
    //LoadBalancingClient lbc;
    //room info
    public static PhotonRoom room;

    public static GameObject LocalPlayerInstance;

    private void Awake()
    {
        if (photonView.IsMine)
        {
            LocalPlayerInstance = this.gameObject;
        }
        if (PhotonRoom.room == null)
        {
            PhotonRoom.room = this;
        }
        else
        {
            if (PhotonRoom.room != this)
            {
                Destroy(PhotonRoom.room.gameObject);
                PhotonRoom.room = this;
            }
        }
        PV = GetComponent<PhotonView>();
        DontDestroyOnLoad(this.gameObject);
    }
    void Start()
    {
        PV = GetComponent<PhotonView>();
    }
    public override void OnEnable()
    {
        base.OnEnable();
        PhotonNetwork.AddCallbackTarget(this);
        SceneManager.sceneLoaded += OnSceneFinishLoading;
    }

    public override void OnDisable()
    {
        base.OnDisable();
        PhotonNetwork.RemoveCallbackTarget(this);
        SceneManager.sceneLoaded -= OnSceneFinishLoading;
    }

    void OnSceneFinishLoading(Scene scene, LoadSceneMode mode)
    {
        currentScene = scene.buildIndex;
        if (currentScene == 1)
        {
            {
                Invoke("CreatePlayer", 3f);
            }
        }
    }

    public override void OnJoinedRoom()
    {
        base.OnJoinedRoom();
        {
            StartGame();
        }
    }

    void StartGame()
    {
        if (!PhotonNetwork.IsMasterClient)
        {
            return;
        }
        PhotonNetwork.LoadLevel(1);
    }

    private void CreatePlayer()
    {
        PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs", "Character"), new Vector3(0, 20, 0f), Quaternion.identity);
    }

    public PhotonPlayer GetPlayerByViewID(int viewID) {
        for (int i = 0; i < playersOnline.Count; i++)
        {
            if (playersOnline[i].GetPV.ViewID == viewID)
            {
                return playersOnline[i];
            }
        }
        return null;
    }
}
public class Session : MonoBehaviour
{
    public PhotonView PV;
    public string playerName;

    public void Initialize(PhotonView PV, string playerName)
    {
        this.PV = PV;
        this.playerName = playerName;
    }
}