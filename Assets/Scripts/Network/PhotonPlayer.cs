﻿using Command;
using Observer;
using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class PhotonPlayer : Entity
{
    PhotonView PV;
    public Renderer renderer;

    public PhotonView GetPV { get => PV; set => PV = value; }

    public GameObject bullet;
    public GameObject fakeBullet;    

    public GameObject arrow;
    public GameObject fakeArrow;

    protected new void Awake() 
    {
        base.Awake();
        PV = GetComponent<PhotonView>();
    }
    protected new void Start()
    {
        base.Start();
        PhotonRoom.room.playersOnline.Add(this);
        if (PV.IsMine)
        {
            renderer.material.SetColor("_BaseColor", Color.green);
            PV.RPC("RPC_Sendmyname", RpcTarget.AllBuffered);

            Layout._instance.InitializeLayout(this);
            PhotonRoom.room.myAvatar = this;
        }
        else
        {
            renderer.material.SetColor("_BaseColor", Color.red);
        }
        for (int i = 0; i < 5; i++)
        {
            SpawnRandomFoodAndRandomLoot();
        }
        if (PhotonNetwork.IsMasterClient) {
            StartCoroutine(ServerUpdate());
            PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs", "Penguin"), new Vector3(5, 20, 5f), Quaternion.identity);
        }
    }
    protected new void Update()
    {
        if (!PV.IsMine)
        {
            return;
        }
        base.Update();
    }
    protected new void LateUpdate()
    {
        if (!PV.IsMine)
        {
            return;
        }
        base.LateUpdate();
    }

    IEnumerator ServerUpdate()
    {
        WaitForSeconds w8 = new WaitForSeconds(10f);
        while (true)
        {
            yield return w8;
            SpawnRandomFoodAndRandomLoot();
        }
    }

    private void SpawnRandomFoodAndRandomLoot() {
            int randomID = Random.Range(0, 10000);
            int randomID2 = Random.Range(0, 10000);
            Vector3 randomPos = new Vector3(Random.Range(-12f, 22), 11, Random.Range(-20, 30));
            Vector3 randomPos2 = new Vector3(Random.Range(-12f, 22), 10, Random.Range(-20, 30));
            //instantiate droploot
            PV.RPC("RPC_DropLoot", RpcTarget.AllBuffered,
                3, randomID, randomPos);
            //instantiate random food.
            PV.RPC("RPC_DropLoot", RpcTarget.AllBuffered,
                    Random.Range(0, Spawner._instance.possibleFoods.Count), randomID2, randomPos2);
        }

    private void OnCollideWithDamageable() 
    {
        TakeDamage(1);
        rb.AddForce(Vector3.up * 400);
    }
    private void OnColliderWithFood() 
    {
        Heal(1);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Food")) {
            int id = Spawner._instance.FindID(other.gameObject);
            PV.RPC("RPC_TakeFood", RpcTarget.AllBuffered, PV.ViewID, 1, id);
        }
    }
    private void OnCollisionEnter(Collision collision)
    {//server
        if (!PhotonNetwork.IsMasterClient) {
            return;
        }
        if (collision.gameObject.CompareTag("Damageable"))
        {
            PV.RPC("RPC_Damage", RpcTarget.AllBuffered, PV.ViewID);
        }
        else if (collision.gameObject.CompareTag("Box")) {
            PV.RPC("RPC_DestroyItem", RpcTarget.All, Spawner._instance.FindID(collision.gameObject));
            int qtd = Random.Range(0, 10);
            int ammo = Random.Range(0, 2);
            AddBullet(qtd,ammo);
        }
    }
    public void SendRPCChangeWeapon() {
        if (!PV.IsMine)
            return;

        int viewID = PV.ViewID;
        PV.RPC("RPC_ChangeWeapon", RpcTarget.Others, viewID);
    }
    public void SendRPCShot(int type) {
        if (!PV.IsMine)
            return;
        int viewID = PV.ViewID;
        PV.RPC("RPC_Shot", RpcTarget.All, viewID, shotStartPoint.transform.position, aim.rotation, type);
    }
    #region RPCS
    [PunRPC]
    void RPC_Damage(int viewID)
    {
        PhotonRoom.room.GetPlayerByViewID(viewID).OnCollideWithDamageable();
    }
    [PunRPC]
    void RPC_TakeFood(int viewID, int amount,int itemID)
    {
        PhotonRoom.room.GetPlayerByViewID(viewID).TakeFood(amount, itemID);
    }
    [PunRPC]
    void RPC_Sendmyname()
    {
        Session session = gameObject.AddComponent<Session>();
        session.Initialize(PV, "PLAYERNAME");
        PhotonRoom.room.sessionsOnline.Add(session);
        Layout._instance.AddNewPlayer(PV.ViewID);
    }
    [PunRPC]
    void RPC_DropLoot(int lootIndex,int LootId, Vector3 position)
    {
        Spawner._instance.SpawnIten(lootIndex, LootId, position);
    }
    [PunRPC]
    void RPC_DropLoot(int lootIndex, int LootId, Vector3 position, int objToDestroyID)
    {
        Spawner._instance.DestroyItemByID(objToDestroyID);
        Spawner._instance.SpawnIten(lootIndex, LootId, position);
    }
    [PunRPC]
    void RPC_ChangeWeapon(int viewID) {
        PhotonPlayer player = PhotonRoom.room.GetPlayerByViewID(viewID);

        var changeWeapponCommand = new ChangeWeaponCommand(this, player.Staff, player.Bow,player);
        player.GetComponent<CommandProcessor>().ExecuteCommand(changeWeapponCommand);
    }
    [PunRPC]
    void RPC_Shot(int viewID, Vector3 position, Quaternion rotation,int bulletType)
    {
        GameObject fake = (bulletType == 0) ? fakeBullet : fakeArrow;
        GameObject original = (bulletType == 0) ? bullet : arrow;

        if (!PhotonNetwork.IsMasterClient) {
            Instantiate(fake, position, rotation);
        }
        else {//IS SERVER
            Instantiate(original, position, rotation);
        }
    }
    [PunRPC]
    void RPC_DestroyItem(int objToDestroyID) {
        Spawner._instance.DestroyItemByID(objToDestroyID);
    }
    #endregion
}



