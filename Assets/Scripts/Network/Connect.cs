﻿using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Connect : ConnectAndJoinRandomLb
{
    private bool isGameStarted = false;

    public new void OnJoinedRoom()
    {
        Debug.Log("entrou aqui");
        PhotonNetwork.LoadLevel(1);
    }

    public new void StartGame() {
        if (isGameStarted)
            return;
        base.StartGame();
        isGameStarted = true;
    }
    private new void Update()
    {
        if (!isGameStarted)
            return;
        base.Update();
    }
}
