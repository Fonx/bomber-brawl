﻿using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class PhotonLobby : MonoBehaviourPunCallbacks
{
    public UnityEvent OnConnectToMaster;
    public Text infoText;
    public static PhotonLobby lobby;
    [SerializeField]
    AppSettings appSettings = new AppSettings();
    private void Awake()
    {
        lobby = this;
    }
    void Start()
    {
        Application.targetFrameRate = 60;
        infoText.text = "connecting";
        PhotonNetwork.ConnectUsingSettings(appSettings);
    }

    public override void OnConnectedToMaster() {
        OnConnectToMaster.Invoke();
        PhotonNetwork.AutomaticallySyncScene = true;
    }
    public void BtnStartPressed() {
        PhotonNetwork.JoinRandomRoom();
    }

    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        base.OnJoinRandomFailed(returnCode, message);
        CreateRoom();
    }
    void CreateRoom() {
        infoText.text = "Creating Room";
        RoomOptions rOps = new RoomOptions() { IsVisible = true, IsOpen = true, MaxPlayers = 10 };
        PhotonNetwork.CreateRoom("Room" + Random.Range(0, 1000), rOps);
    }
    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        base.OnCreateRoomFailed(returnCode, message);
        CreateRoom();
    }
}
