﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Command
{
    public class ScaleCommand : Command
    {
        private readonly float scaleFactor;
        public ScaleCommand(IEntity entity, float scale) : base(entity)
        {
            this.scaleFactor = scale;
        }

        public override void Execute()
        {
            entity.transform.localScale = new Vector3(scaleFactor, scaleFactor, scaleFactor);
        }
        public override void Undo()
        {
            throw new System.NotImplementedException();
        }
    }
}