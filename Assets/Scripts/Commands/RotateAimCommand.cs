﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Command { 
public class RotateAimCommand : Command
    {
        Vector3 target;

        public RotateAimCommand(IEntity entity, Vector3 target) : base(entity)
        {
            this.target = target;
        }

        public override void Execute()
        {
            entity.aim.transform.LookAt(target);
        }

        public override void Undo()
        {
            throw new System.NotImplementedException();
        }
    }
}