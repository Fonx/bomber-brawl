﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Command
{
    public class MoveCommand : Command
    {
        Vector3 direction;

        public MoveCommand(IEntity entity, Vector3 direction) : base(entity)
        {
            this.direction = direction;
        }

        public override void Execute()
        {
            entity.transform.position += direction * 0.05f;
        }

        public override void Undo()
        {
            entity.transform.position -= direction * 0.05f;
        }
    }
}