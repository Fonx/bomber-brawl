﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Observer
{
    public class Layout : SubscriberPlayer
    {
        public List<Sprite> gunsSprite;
        public Text bulletqtd;
        public Text life;
        public Image currentGunImage;
        private bool selectingBow = false;
        public GameObject playerStats;
        public Transform playersStats;
        List<PlayerStats> stats = new List<PlayerStats>();
       
        public static Layout _instance;
        private void Awake()
        {
            if (_instance == null)
            {
                _instance = this;
            }
            else {
                Destroy(gameObject);
            }
        }

        protected override void OnPlayerChangeWeapon()
        {
            if (!selectingBow)
            {
                currentGunImage.sprite = gunsSprite[1];
                bulletqtd.text = players[0].Bullets[1].ToString();
                selectingBow = true;
            }   
            else 
            {
                currentGunImage.sprite = gunsSprite[0];
                bulletqtd.text = players[0].Bullets[0].ToString();
                selectingBow = false;
            }
        }
        protected override void OnPlayerGetBullet()
        {
            if (selectingBow)
            {
                bulletqtd.text = players[0].Bullets[1].ToString();
            }
            else
            {
                bulletqtd.text = players[0].Bullets[0].ToString();
            }
        }
        protected override void OnPlayerHealed(int heal)
        {
            life.text = players[0].CurrentHealth.ToString();
        }
        protected override void OnPlayerDamage(int damage)
        {
            life.text = players[0].CurrentHealth.ToString();
        }
        public void InitializeLayout(Player player)
        {
            SubscribeNewPlayer(player);
        }

        private void UpdatePlayerStats(int playerID, int scoreToAdd) {
            int index = stats.FindIndex(a=>a.PlayerID == playerID);
            if (index >= 0)
            {
                stats[index].UpdateScore(scoreToAdd);
            }
            else {
                Debug.LogError("Cant Find PlayerID " + playerID);
            }
        }

        public void AddNewPlayer(int playerID) {
            PlayerStats newStats = Instantiate(playerStats, playersStats).GetComponent<PlayerStats>();
            newStats.Initialize(playerID);
            stats.Add(newStats);
        }
    }
}
