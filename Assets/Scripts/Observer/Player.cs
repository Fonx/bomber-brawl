﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Observer
{
    public class Player : MonoBehaviour
    {
        public event Action<Vector3,int> Shot = delegate { };
        public event Action<int> Damage = delegate { };
        public event Action<int> Healed = delegate { };
        public event Action Killed = delegate { };
        public event Action<Transform> Move = delegate { };
        public event Action ChangeWeapon = delegate { };
        public event Action UpdateBullet = delegate { };

        private int[] bullets = new int[2] { 5,0};
        public int[] Bullets => bullets;
        int maxHealth = 5;
        public int MaxHealth => maxHealth;
        int currentHealth = 5;
        public int CurrentHealth 
        {
            get=> currentHealth;
            set
            {
                if (value > MaxHealth)
                    value = MaxHealth;

                currentHealth = value;
            }
        }
        [SerializeField] protected GameObject bow;
        [SerializeField] protected GameObject staff;
        public GameObject Bow => bow;
        public GameObject Staff => staff;
        protected void Awake()
        {

        }
        public void OnPlayerShot(Vector3 startPoint, int index) 
        {
            Shot.Invoke(startPoint, index);
        }
        public void TakeDamage(int amount)
        {
            Damage.Invoke(amount);
            currentHealth--;
            if (currentHealth < 0) 
            {
                OnPlayerKilled();
            }
        }
        public void Heal(int amount)
        {
            Healed.Invoke(amount);
            if (currentHealth >= MaxHealth)
                return;
            currentHealth++;
        }
        public void TakeFood(int amount, int foodID) {
            Heal(amount);
            Spawner._instance.DestroyItemByID(foodID);
        }
        protected void OnPlayerMove() 
        {
            Move.Invoke(transform);
        }

        public void OnPlayerKilled() 
        {
            Killed.Invoke();
        }
        public void OnChangeWeapon() {
            ChangeWeapon.Invoke();
        }

        protected void AddBullet(int qtd, int type) {
            Bullets[type] += qtd;
            UpdateBullet.Invoke();
        }
        public void RemoveBullet(int type) {
            Bullets[type]--;
            UpdateBullet.Invoke();
        }
    }
}
