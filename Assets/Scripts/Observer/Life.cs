﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Observer
{
    public class Life : SubscriberPlayer
    {
        TextMesh text;
        void Start()
        {
            text = GetComponent<TextMesh>();
        }

        protected override void OnPlayerDamage(int damage)
        {
            text.text = players[0].CurrentHealth.ToString();
        }

        protected override void OnPlayerHealed(int damage)
        {
            text.text = players[0].CurrentHealth.ToString();
        }
        protected override void OnPlayerWalk(Transform location)
        {

        }
        protected override void OnPlayerDead()
        {

        }
        private void OnEnable()
        {
            for (int i = 0; i < players.Count; i++)
            {
                SubscribePlayer(players[i]);
            }
        }
    }
}