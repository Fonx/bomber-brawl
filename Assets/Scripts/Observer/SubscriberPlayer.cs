﻿using Observer;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Observer
{
    public class SubscriberPlayer : MonoBehaviour
    {
        public List<Player> players = new List<Player>();

        #region CALLBACKS
        protected virtual void OnPlayerDamage(int damage) { }
        protected virtual void OnPlayerHealed(int heal) { }
        protected virtual void OnPlayerWalk(Transform location) { }
        protected virtual void OnPlayerDead() { }
        protected virtual void OnPlayerShot(Vector3 startPosition, int index) { }
        protected virtual void OnPlayerChangeWeapon() { }
        protected virtual void OnPlayerGetBullet() { }
        #endregion
        protected void SubscribePlayer(Player player)
        {
            player.Damage += OnPlayerDamage;
            player.Move += OnPlayerWalk;
            player.Healed += OnPlayerHealed;
            player.Killed += OnPlayerDead;
            player.Shot += OnPlayerShot;
            player.ChangeWeapon += OnPlayerChangeWeapon;
            player.UpdateBullet += OnPlayerGetBullet;
        }
        protected void UnsubscribePlayer(Player player)
        {
            player.Damage -= OnPlayerDamage;
            player.Move -= OnPlayerWalk;
            player.Healed -= OnPlayerHealed;
            player.Killed -= OnPlayerDead;
        }
        public void SubscribeNewPlayer(Player player)
        {
            SubscribePlayer(player);
            players.Add(player);
        }
        public void RemovePlayer(Player player)
        {
            UnsubscribePlayer(player);
            players.Remove(player);
        }

    }
}