﻿namespace Command
{
    public class ShotCommand : Command
    {
        PhotonPlayer player;
        public ShotCommand(IEntity entity, PhotonPlayer player) : base(entity)
        {
            this.player = player;
        }

        public override void Execute()
        {
            int type = (player.Staff.activeSelf) ? 0 : 1;
            if (player.Bullets[type] > 0)
            {
                player.RemoveBullet(type);
                player.SendRPCShot(type);
            }
            else {
                
            }
        }

        public override void Undo()
        {

        }
    }
}