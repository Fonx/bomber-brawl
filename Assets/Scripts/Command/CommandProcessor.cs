﻿using System.Collections.Generic;
using UnityEngine;

namespace Command
{
    public class CommandProcessor : MonoBehaviour
    {
        List<Command> commands = new List<Command>();
        int currentCommandHead = 0;
        PhotonPlayer photonPlayer;
        private void Start()
        {
            photonPlayer = GetComponent<PhotonPlayer>();
        }
        public void ExecuteCommand(Command command)
        {
            commands.Add(command);
            command.Execute();
            currentCommandHead = commands.Count - 1;
        }

        public void Undo()
        {
            if (currentCommandHead < 0)
                return;
            commands[currentCommandHead].Undo();
            commands.RemoveAt(currentCommandHead);
        }
        public void Undo(int commandIndex)
        {
            if (commandIndex > currentCommandHead)
            {
                Debug.LogError("Cant Find Command");
                return;
            }
            commands[commandIndex].Undo();
            commands.RemoveAt(commandIndex);
        }

        public void Redo()
        {
            commands[currentCommandHead].Execute();
            currentCommandHead++;
        }
    }
}