﻿using UnityEngine;

namespace Command
{
    public class ChangeWeaponCommand : Command
    {
        GameObject staff;
        GameObject bow;
        PhotonPlayer player;
        public ChangeWeaponCommand(IEntity entity, GameObject staff, GameObject bow, PhotonPlayer player) : base(entity)
        {
            this.staff = staff;
            this.bow = bow;
            this.player = player;
        }

        public override void Execute()
        {
            if (staff.activeSelf)
            {
                staff.SetActive(false);
                bow.SetActive(true);
            }
            else {
                staff.SetActive(true);
                bow.SetActive(false);
            }
            player.SendRPCChangeWeapon();
        }

        public override void Undo()
        {

        }
    }
}

