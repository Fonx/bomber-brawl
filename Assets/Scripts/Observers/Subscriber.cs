﻿using Observer;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Subscriber : MonoBehaviour
{
    protected List<Player> players = new List<Player>();

    protected void SubscribePlayer(Player player)
    {
        player.Damage += OnPlayerDamage;
        player.Move += OnPlayerWalk;
        player.Healed += OnPlayerHealed;
        player.Killed += OnPlayerDead;
    }
    protected void UnsubscribePlayer(Player player)
    {
        player.Damage -= OnPlayerDamage;
        player.Move -= OnPlayerWalk;
        player.Healed -= OnPlayerHealed;
        player.Killed -= OnPlayerDead;
    }
    public void SubscribeNewPlayer(Player player)
    {
        SubscribePlayer(player);
        players.Add(player);
    }
    public void RemovePlayer(Player player)
    {
        UnsubscribePlayer(player);
        players.Remove(player);
    }

    #region CALLBACKS
    protected virtual void OnPlayerDamage(int damage) { }
    protected virtual void OnPlayerHealed(int heal) { }
    protected virtual void OnPlayerWalk(Transform location) { }
    protected virtual void OnPlayerDead() { }
    #endregion
}
