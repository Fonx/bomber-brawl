﻿using System;
using UnityEngine;

namespace Observer
{
    public class Player : MonoBehaviour
    {
        public event Action<int> Damage = delegate { };
        public event Action<int> Healed = delegate { };
        public event Action Killed = delegate { };

        public event Action<Transform> Move = delegate { };

        int maxHealth = 100;
        int currentHealth = 100;
        public int MaxHealth => maxHealth;
        public int CurrentHealth 
        {
            get=> currentHealth;
            set{
                if (value > MaxHealth)
                    value = MaxHealth;

                currentHealth = value;
            }
        }

        public void TakeDamage(int amount)
        {
            Damage.Invoke(amount);
        }

        protected void OnPlayerMove() {
            Move.Invoke(transform);
        }
    }
}
