﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Observer
{
    public class PlaySound : Subscriber
    {
        [SerializeField] AudioClip walkSound;
        [SerializeField] AudioClip shotSound;

        AudioSource audioSource;
        private bool playingWalkSoundRotine;

        private void Awake()
        {
            audioSource = GetComponent<AudioSource>();
        }
        /*
        private void OnEnable()
        {
            for (int i=0;i<players.Count;i++) {
                SubscribePlayer(players[i]);
            }
        }
        private void OnDisable()
        {
            for (int i = 0; i < players.Count; i++)
            {
                UnsubscribePlayer(players[i]);
            }
        }
        */
        protected new void OnPlayerDamage(int damage) {
        
        }
        protected new void OnPlayerHealed(int heal)
        {

        }
        protected new void OnPlayerWalk(Transform location) {
            if (walkSound &&
                location && 
                !audioSource.isPlaying &&
                !playingWalkSoundRotine)
            {
                StartCoroutine(PlayWalkSoundRotine(location.position));
            }
        }
        protected new void OnPlayerDead() {
        
        }

        IEnumerator PlayWalkSoundRotine(Vector3 position) {
            playingWalkSoundRotine = true;
            WaitForSeconds w8 = new WaitForSeconds(walkSound.length);
            AudioSource.PlayClipAtPoint(walkSound, position);
            yield return w8;
            playingWalkSoundRotine = false;
        }
    }
}
