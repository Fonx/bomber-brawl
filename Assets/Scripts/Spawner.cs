﻿using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public List<(int,GameObject)> spawnedItens = new List<(int, GameObject)>();
    public List<Item> possibleFoods;
    public Item box;
    public static Spawner _instance;

    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
        }
        else {
            Destroy(gameObject);
        }
    }

    public void SpawnIten(int index,int ID,Vector3 position) {//mudar
        if (index < 3) {
            for (int i = 0; i < possibleFoods.Count; i++)
            {
                if (possibleFoods[i].id == index)
                {
                    GameObject food = Instantiate(possibleFoods[i].prefab, position, Quaternion.identity);
                    spawnedItens.Add((ID, food));
                    return;
                }
            }
            Debug.LogError("Cant Find Index " + index);
        }

        else if (index == 3) {
            SpawnBox(ID,position);
        }
    }
    public void DestroyItemByID(int ID) {
        int index = spawnedItens.FindIndex(a => a.Item1 == ID);
        Destroy(spawnedItens[index].Item2);
        spawnedItens.RemoveAt(index);
    }
    public void SpawnBox(int ID, Vector3 position) {
        GameObject boxGO = Instantiate(box.prefab,position,Quaternion.identity);
        spawnedItens.Add((ID, boxGO));
    }
    public int FindID(GameObject gameObj) {
        int index = spawnedItens.FindIndex(a => a.Item2 == gameObj);
        return spawnedItens[index].Item1;
    }

}
