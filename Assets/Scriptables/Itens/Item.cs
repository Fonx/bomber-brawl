﻿using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/Item", order = 0)]
public class Item : ScriptableObject
{
    public int id;
    public GameObject prefab;
    public Sprite sprite;
    public string description;
}