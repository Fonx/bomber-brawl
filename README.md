# Bomber Brawl

## Motivation

This project was created to serve as a module for future applications that use design patterns (command and observer) and network (to sync characteres).

I chose to use photon for the low complexity but its possible to adjust to use other network Tecnologies such as FB Rtdb and/or Mirror.

Example of Game created using BomberBrawl:
https://avatarfight.vercel.app/



## Game ProgrammingPatterns
https://gameprogrammingpatterns.com/contents.html

Two main design patterns used in this project: 
- Command pattern
- Observer pattern


## Photon DashBoard to Get AppID
set AppID on '''Scripts''' GameObject on Load-Scene

https://gcdn.pbrd.co/images/VQr30zmt8JYW.png?o=1

https://dashboard.photonengine.com/en-US/
